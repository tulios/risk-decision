# Risk Decision

You are to implement a REST service in Java for a simplified risk decision process.

Assumptions and Limitations:
  * The state of the service does not need to be persistent.
  * Java is used, but you may freely choose frameworks and tools.
  
API calls:

There is one REST endpoint that represents making risk assessment of a purchase.
POST /decision

## Dependencies

Java 1.8

## Install

```sh
mvn install
```

## Usage

It will require a [Servlet container](http://tomcat.apache.org/download-80.cgi)

```sh
curl -H 'Content-Type: application/json' -d '{"email": "a@b.se", "first_name" : "a", "last_name": "b", "amount": 100}' http://localhost:8080/decision
# {"accepted":true,"reason":"ok"}
```

## Tests

```sh
mvn clean test
```