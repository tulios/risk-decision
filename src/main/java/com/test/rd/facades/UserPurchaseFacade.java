package com.test.rd.facades;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import com.test.rd.domain.PurchaseAnalysis;
import com.test.rd.domain.UserPurchase;
import com.test.rd.domain.PurchaseAnalysis.Reason;

public class UserPurchaseFacade {
	
	private static UserPurchaseFacade instance;
	
	public static UserPurchaseFacade getInstance() {
		if (instance == null) {
			instance = new UserPurchaseFacade();
		}
		return instance;
	}
	
	private AtomicInteger atomicCounter;
	private ConcurrentHashMap<String, Integer> storage;
	
	private UserPurchaseFacade() {
		atomicCounter = new AtomicInteger();
		storage = new ConcurrentHashMap<String, Integer>();
	}
	
	public int insertOrUpdate(UserPurchase userPurchase) {
		int id = generateId();
		String scopedEmail = generateScopedEmail(userPurchase.getEmail(), id);
		storage.put(scopedEmail, userPurchase.getAmount());
		return id;
	}
		
	public int calculateTotalAmount(String email) {
		Pattern pattern = Pattern.compile(Pattern.quote(email) + "\\|[0-9]+$");
		
		long parallelismThreshold = 1L;
		int initialValue = 0;
		return storage.reduceToInt(
			parallelismThreshold,
			(String scopedMail, Integer amount) -> {
				return pattern.matcher(scopedMail).matches() ? amount : 0; 
			},
			initialValue,
			Integer::sum
		);
	}
	
	public PurchaseAnalysis generateAnalysis(UserPurchase userPurchase) {
		if (userPurchase.getAmount() <= 0) {
			return PurchaseAnalysis.reject(Reason.INVALID);
		}
		
		if (userPurchase.getAmount() > 1000) {
			return PurchaseAnalysis.reject(Reason.AMOUNT);
		}
		
		int totalPurchased = UserPurchaseFacade.
							 getInstance().
							 calculateTotalAmount(userPurchase.getEmail());
		
		if ((totalPurchased + userPurchase.getAmount()) > 1000) {
			return PurchaseAnalysis.reject(Reason.DEBT);
		}
		
		insertOrUpdate(userPurchase);
		return PurchaseAnalysis.accept();
	}
	
	protected ConcurrentHashMap<String, Integer> getStorage() {
		return storage;
	}
	
	protected String generateScopedEmail(String email, int id) {
		return email + "|" + id;
	}
	
	private int generateId() {
		return atomicCounter.incrementAndGet();
	}
	
}
