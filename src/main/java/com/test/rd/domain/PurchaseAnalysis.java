package com.test.rd.domain;

public class PurchaseAnalysis {
	private boolean accepted;
	private String reason;
	
	public PurchaseAnalysis() {
	}
	
	public PurchaseAnalysis(boolean accepted, String reason) {
		this.accepted = accepted;
		this.reason = reason;
	}
	
	public boolean isAccepted() {
		return accepted;
	}
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	public static PurchaseAnalysis reject(Reason reason) {
		return new PurchaseAnalysis(false, reason.getMessage());
	}
	
	public static PurchaseAnalysis accept() {
		return new PurchaseAnalysis(true, Reason.OK.getMessage());
	}
	
	public enum Reason {
		OK("ok"),
		AMOUNT("amount"),
		DEBT("debt"),
		INVALID("invalid");
		
		private String message;
		
		Reason(String message) {
			this.message = message;
		}
		
		public String getMessage() {
			return this.message;
		}
	}
}
