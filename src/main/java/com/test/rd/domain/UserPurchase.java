package com.test.rd.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserPurchase {
	private String email;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("last_name")
	private String lastName;

	private int amount;
	
	public UserPurchase() {
	}
	
	public UserPurchase(String email, String firstName, String lastName, int amount) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
