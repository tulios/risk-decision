package com.test.rd;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;

import com.test.rd.domain.PurchaseAnalysis;
import com.test.rd.domain.UserPurchase;
import com.test.rd.facades.UserPurchaseFacade;

@Path("decision")
public class RiskDecisionResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public PurchaseAnalysis analyze(UserPurchase userPurchase) {
    	return UserPurchaseFacade.
    			getInstance().
    			generateAnalysis(userPurchase);
    }
    
}
