package com.test.rd.facades;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.Before;
import org.junit.Test;

import com.test.rd.domain.PurchaseAnalysis;
import com.test.rd.domain.UserPurchase;
import com.test.rd.domain.PurchaseAnalysis.Reason;

import static org.junit.Assert.*;

public class UserPurchaseFacadeTest {
	
	@Before
	public void setup() {
		getStorage().clear();
	}
	
	@Test
	public void insertNewPurchasesWithScopedEmailAndReturnId() {
		UserPurchase userPurchase = createUserPurchase(100);
		int id = getFacade().insertOrUpdate(userPurchase);
		String scopedEmail = getFacade().generateScopedEmail(userPurchase.getEmail(), id);
		
		assertTrue(id > 0);
		assertTrue(getStorage().containsKey(scopedEmail));
		assertTrue(getStorage().contains(userPurchase.getAmount()));
	}
	
	@Test
	public void insertNewPurchasesForTheSameEmail() {
		UserPurchase userPurchase = createUserPurchase(100);
		int id1 = getFacade().insertOrUpdate(userPurchase);
		
		userPurchase.setAmount(50);
		int id2 = getFacade().insertOrUpdate(userPurchase);
		
		userPurchase.setAmount(25);
		int id3 = getFacade().insertOrUpdate(userPurchase);
		
		assertTrue(id1 != id2);
		assertTrue(id1 != id3);
		assertTrue(id2 != id3);
		assertEquals(getStorage().size(), 3);
		assertTrue(
			getStorage().containsKey(
				getFacade().generateScopedEmail(userPurchase.getEmail(), id1)
			)
		);
		assertTrue(
			getStorage().containsKey(
				getFacade().generateScopedEmail(userPurchase.getEmail(), id2)
			)
		);
		assertTrue(
			getStorage().containsKey(
				getFacade().generateScopedEmail(userPurchase.getEmail(), id3)
			)
		);
	}
	
	@Test
	public void calculatesTheTotalAmountOfAGivenEmail() {
		UserPurchase userPurchase = createUserPurchase(100);
		assertEquals(
			0,
			getFacade().calculateTotalAmount(userPurchase.getEmail())
		);
		
		getFacade().insertOrUpdate(userPurchase);
		userPurchase.setAmount(50);
		getFacade().insertOrUpdate(userPurchase);
		userPurchase.setAmount(25);
		getFacade().insertOrUpdate(userPurchase);
		
		assertEquals(
			175,
			getFacade().calculateTotalAmount(userPurchase.getEmail())
		);
	}
	
	@Test
	public void rejectsPurchasesWhenAmountIsNegativeOrZero() {
		UserPurchase userPurchase = createUserPurchase(-1);
		PurchaseAnalysis analysis = getFacade().generateAnalysis(userPurchase);
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.INVALID.getMessage(), analysis.getReason());
		
		userPurchase.setAmount(0);
		analysis = getFacade().generateAnalysis(userPurchase);
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.INVALID.getMessage(), analysis.getReason());
	}
	
	@Test
	public void acceptsAndSavesPurchaseWhenAmountIsBelow10AndGreaterThanZero() {
		UserPurchase userPurchase = createUserPurchase(1);
		assertEquals(0, getStorage().size());
		
		for (int i = 1; i < 10; i++) {
			userPurchase.setAmount(i);
			PurchaseAnalysis analysis = getFacade().generateAnalysis(userPurchase);
			assertTrue(analysis.isAccepted());
			assertEquals(Reason.OK.getMessage(), analysis.getReason());
		}
		
		assertEquals(9, getStorage().size());
	}
	
	@Test
	public void rejectsPurchasesWhenAmountIsGreaterThan1000() {
		UserPurchase userPurchase = createUserPurchase(1001);
		PurchaseAnalysis analysis = getFacade().generateAnalysis(userPurchase);
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.AMOUNT.getMessage(), analysis.getReason());
	}
	
	@Test
	public void rejectsPurchasesWhenTotalAmountIsGreaterThan1000() {
		UserPurchase userPurchase = createUserPurchase(1000);
		PurchaseAnalysis analysis = getFacade().generateAnalysis(userPurchase);
		assertTrue(analysis.isAccepted());
		assertEquals(Reason.OK.getMessage(), analysis.getReason());
		
		userPurchase.setAmount(1);
		analysis = getFacade().generateAnalysis(userPurchase);
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.DEBT.getMessage(), analysis.getReason());
	}
	
	@Test
	public void acceptsAndSavesPurchaseWhenAmountIsBellow1000() {
		UserPurchase userPurchase = createUserPurchase(10);
		
		for (int i = 10; i < 1000; i++) {
			getStorage().clear();
			userPurchase.setAmount(i);
			PurchaseAnalysis analysis = getFacade().generateAnalysis(userPurchase);
			assertTrue(analysis.isAccepted());
			assertEquals(Reason.OK.getMessage(), analysis.getReason());
		}
	}
	
	private UserPurchase createUserPurchase(int amount) {
		return new UserPurchase("a@b.se", "a", "b", amount);
	}
	
	private ConcurrentHashMap<String, Integer> getStorage() {
		return getFacade().getStorage();
	}
	
	private UserPurchaseFacade getFacade() {
		return UserPurchaseFacade.getInstance();
	}
	
}
