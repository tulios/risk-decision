package com.test.rd;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Application;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;

import com.test.rd.domain.PurchaseAnalysis;
import com.test.rd.domain.UserPurchase;
import com.test.rd.domain.PurchaseAnalysis.Reason;

import org.junit.Test;

import static org.junit.Assert.*;

public class RiskDecisionResourceTest extends JerseyTest {
	
	protected Application configure() {
        return new ResourceConfig(RiskDecisionResource.class);
    }
	
	@Test
	public void requestThatAcceptsPurchase() {
		PurchaseAnalysis analysis = requestDecision(new UserPurchase("a@b.se", "a", "b", 100));
		
		assertTrue(analysis.isAccepted());
		assertEquals(Reason.OK.getMessage(), analysis.getReason());
	}
	
	@Test
	public void requestThatRejectsPurchase() {
		PurchaseAnalysis analysis = requestDecision(new UserPurchase("a@b.se", "a", "b", 1001));
		
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.AMOUNT.getMessage(), analysis.getReason());
	}
	
	private PurchaseAnalysis requestDecision(UserPurchase userPurchase) {
		return target("decision").
			request(MediaType.APPLICATION_JSON_TYPE).
			post(
				Entity.entity(userPurchase, MediaType.APPLICATION_JSON_TYPE),
				PurchaseAnalysis.class
			);
	}

}
