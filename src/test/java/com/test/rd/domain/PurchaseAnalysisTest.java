package com.test.rd.domain;

import com.test.rd.domain.PurchaseAnalysis;
import com.test.rd.domain.PurchaseAnalysis.Reason;

import org.junit.Test;

import static org.junit.Assert.*;

public class PurchaseAnalysisTest {
	
	@Test
	public void methodRejectReturnsUnacceptedAnalysisWithTheGivenReason() {
		PurchaseAnalysis analysis = PurchaseAnalysis.reject(Reason.INVALID);
		assertFalse(analysis.isAccepted());
		assertEquals(Reason.INVALID.getMessage(), analysis.getReason());
	}
	
	@Test
	public void methodAcceptReturnsAcceptedAnalysisWithOkReason() {
		PurchaseAnalysis analysis = PurchaseAnalysis.accept();
		assertTrue(analysis.isAccepted());
		assertEquals(Reason.OK.getMessage(), analysis.getReason());
	}

}
